package com.isen.teaching.checkers.api.model;

/**
 * Created by A613792 on 16/01/2017.
 */
public enum Direction {

    UP, DOWN, BOTH

}
