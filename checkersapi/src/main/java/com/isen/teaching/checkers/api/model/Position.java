package com.isen.teaching.checkers.api.model;

/**
 * Created by A613792 on 16/01/2017.
 */
public class Position {

    private Integer columnIndex;

    private Integer rowIndex;

    public Position() {
    }

    public Position(int columnIndex, int rowIndex)
    {
        this.columnIndex = columnIndex;
        this.rowIndex = rowIndex;
    }

    public Integer getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(Integer columnIndex) {
        this.columnIndex = columnIndex;
    }

    public Integer getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(Integer rowIndex) {
        this.rowIndex = rowIndex;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Position) {
            Position other = (Position) obj;
            return other.getColumnIndex().equals(this.getColumnIndex()) && other.getRowIndex().equals(this.getRowIndex());
        }
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return new StringBuilder().append("(").append(columnIndex).append(",").append(rowIndex).append(")").toString();
    }

    public Position translate(Position direction, int step) {
        return new Position(this.columnIndex + direction.columnIndex*step, this.rowIndex + direction.rowIndex*step);
    }
}
