package com.isen.teaching.checkers.api.model.move;

import com.isen.teaching.checkers.api.model.cell.Cell;
import com.isen.teaching.checkers.api.model.pawn.Pawn;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by A613792 on 16/01/2017.
 */
public class Move {

    private Cell destination;
    private Cell deletePawn;

    public Move(Cell destination) {
        this.destination = destination;
    }

    public Move(Cell destination, Cell pawnCellToDelete) {
        this(destination);
        this.deletePawn = pawnCellToDelete;
    }

    public Move() {
    }

    public Cell getDestination() {
        return destination;
    }

    public void setDestination(Cell destination) {
        this.destination = destination;
    }

    public Cell getDeletePawn() {
        return deletePawn;
    }

    public void setDeletePawn(Cell deletePawn) {
        this.deletePawn = deletePawn;
    }

    public boolean hasPawnToDelete() {
        return this.deletePawn != null;
    }

    public boolean isMandatory() {
        return hasPawnToDelete();
    }

    public Cell getPawnCellToDelete() {
        return deletePawn;
    }
}
