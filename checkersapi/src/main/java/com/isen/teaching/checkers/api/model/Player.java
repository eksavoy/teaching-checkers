package com.isen.teaching.checkers.api.model;

import com.isen.teaching.checkers.api.model.pawn.CustomPawn;
import com.isen.teaching.checkers.api.model.pawn.Pawn;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by A613792 on 16/01/2017.
 */
public class Player {
    private final Direction pawnDirection;
    private final int queenRow;
    private int nbPawns;
    private Color colorPawn;

    public Player(int nbPawns, Color colorPawn, Direction pawnDirection, int queenRow) {
        this.nbPawns = nbPawns;
        this.colorPawn = colorPawn;
        this.pawnDirection = pawnDirection;
        this.queenRow = queenRow;
    }

    public int getNbPawns() {
        return nbPawns;
    }

    public void removePawn(Pawn pawn) {
        this.nbPawns--;
    }

    public Direction getPawnDirection() {
        return pawnDirection;
    }

    public int getQueenRow() {
        return queenRow;
    }

    public Color getColorPawn() {
        return colorPawn;
    }

    public Pawn newPawn(Color color, Direction direction) {
        return new CustomPawn(color, direction);
    }
}
