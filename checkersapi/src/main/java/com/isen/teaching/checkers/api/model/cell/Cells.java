package com.isen.teaching.checkers.api.model.cell;

import com.isen.teaching.checkers.api.exception.UnauthorizedMoveException;
import com.isen.teaching.checkers.api.model.Position;
import com.isen.teaching.checkers.api.model.move.Move;
import com.isen.teaching.checkers.api.model.pawn.Pawn;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by A613792 on 16/01/2017.
 */
public class Cells {

    List<List<Cell>> cells;

    private int nbColumns;
    private int nbRows;

    public Cells(int nbColumns, int nbRows) {
        this.nbColumns = nbColumns;
        this.nbRows = nbRows;

        cells = new ArrayList<>(nbColumns);

        for (int column = 0; column < nbColumns; column++){
            cells.add(new ArrayList<>(nbRows));
            for (int row = 0; row < nbRows; row++){
                this.cells.get(column).add(new Cell(column, row));
            }
        }
    }

    public List<List<Cell>> getCells() {
        return cells;
    }

    public void setCells(List<List<Cell>> cells) {
        this.cells = cells;
    }

    public List<Cell> getColumn(int col) {
        return this.cells.get(col);
    }

    public Cell get(Position position) {
        return get(position.getColumnIndex(), position.getRowIndex());
    }

    public Cell get(int col, int row) {
        return this.getColumn(col).get(row);
    }

    public int getNbColumns() {
        return this.nbColumns;
    }

    public int getNbRows() {
        return this.nbRows;
    }


    public Cell translate(Cell cell, Position direction, int step) {
        return this.get(cell.getPosition().translate(direction, step));
    }

    public int getLastRowIndex() {
        return this.nbRows - 1;
    }
}
