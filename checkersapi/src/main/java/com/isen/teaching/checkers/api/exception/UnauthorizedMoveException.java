package com.isen.teaching.checkers.api.exception;

import com.isen.teaching.checkers.api.model.move.Move;

import java.util.List;

/**
 * Created by A613792 on 16/01/2017.
 */
public class UnauthorizedMoveException extends Exception {
    private List<Move> mandatoryDestination;

    public UnauthorizedMoveException(List<Move> mandatoryDestination)
    {
        this.mandatoryDestination = mandatoryDestination;
    }

    public List<Move> getMandatoryDestination() {
        return mandatoryDestination;
    }

    public void setMandatoryDestination(List<Move> mandatoryDestination) {
        this.mandatoryDestination = mandatoryDestination;
    }
}
