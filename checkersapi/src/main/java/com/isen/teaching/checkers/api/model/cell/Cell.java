package com.isen.teaching.checkers.api.model.cell;

import com.isen.teaching.checkers.api.model.Color;
import com.isen.teaching.checkers.api.model.pawn.Pawn;
import com.isen.teaching.checkers.api.model.Position;
import org.codehaus.jackson.annotate.JsonIgnore;

public class Cell {

	private Color color;

	private Position position;

	private Pawn currentPawn;

	public Cell(int column, int row) {
		this.position = new Position(column, row);
		this.color = (((row + (column % 2 == 0 ? 0 : 1)) % 2) == 0) ? Color.WHITE : Color.BLACK;
	}

	public boolean hasPawn() {
		return this.currentPawn != null;
	}

	public boolean hasOpponentPawn(Pawn other) {
		return hasPawn() && getCurrentPawn().getColor() != other.getColor();
	}

	public Pawn getCurrentPawn() {
		return currentPawn;
	}

	public void setCurrentPawn(Pawn currentPawn) {
		this.currentPawn = currentPawn;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Position getPosition() {
		return position;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Cell) {
			Cell other = (Cell) obj;
			return this.getPosition().equals(other.getPosition());
		}
		return super.equals(obj);
	}

}
