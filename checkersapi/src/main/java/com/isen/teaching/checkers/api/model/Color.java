package com.isen.teaching.checkers.api.model;

public enum Color {
	WHITE(0), BLACK(1), RED(2), BLUE(3), GREEN(4);

	private int value;

	private Color(int value) {
		this.value = value;
	}

};
