package com.isen.teaching.checkers.api.model.pawn;

import com.isen.teaching.checkers.api.exception.UnauthorizedMoveException;
import com.isen.teaching.checkers.api.model.Color;
import com.isen.teaching.checkers.api.model.Direction;
import com.isen.teaching.checkers.api.model.cell.Cell;
import com.isen.teaching.checkers.api.model.cell.Cells;
import com.isen.teaching.checkers.api.model.move.Move;

import java.util.List;

/**
 * Created by A613792 on 16/01/2017.
 */
public class CustomPawn extends Pawn {

    public CustomPawn(Color color, Direction direction) {
        super(color, direction);
    }
}
