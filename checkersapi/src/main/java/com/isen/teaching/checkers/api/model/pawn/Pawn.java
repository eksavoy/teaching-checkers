package com.isen.teaching.checkers.api.model.pawn;

import com.isen.teaching.checkers.api.exception.UnauthorizedMoveException;
import com.isen.teaching.checkers.api.model.Color;
import com.isen.teaching.checkers.api.model.Direction;
import com.isen.teaching.checkers.api.model.Position;
import com.isen.teaching.checkers.api.model.cell.Cell;
import com.isen.teaching.checkers.api.model.cell.Cells;
import com.isen.teaching.checkers.api.model.move.Move;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by A613792 on 09/01/2017.
 */
public abstract class Pawn {
    private Color color;
    private Direction direction;

    @JsonIgnore
    private Cell cell;


    public Pawn(Color color, Direction direction) {
        this.color = color;
        this.direction = direction;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void setCell(Cell cell) {
        if (this.cell != null) {
            this.cell.setCurrentPawn(null);
        }
        if (cell != null) {
            cell.setCurrentPawn(this);
        } // else : Pawn removed from board
        this.cell = cell;
    }

    public Cell getCell() {
        return cell;
    }

    public void toQueen() {
        this.direction = Direction.BOTH;
    }
}
