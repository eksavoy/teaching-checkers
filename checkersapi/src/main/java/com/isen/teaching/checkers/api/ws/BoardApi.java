package com.isen.teaching.checkers.api.ws;

import com.isen.teaching.checkers.api.model.Board;
import com.isen.teaching.checkers.api.model.Player;
import com.isen.teaching.checkers.api.model.Position;
import com.isen.teaching.checkers.api.model.cell.Cell;
import com.isen.teaching.checkers.api.model.move.Move;

import javax.jws.WebMethod;
import java.util.List;


/**
 * Created by A613792 on 16/01/2017.
 */

public interface BoardApi {

    public List<List<Cell>> getBoard();

    public Player getPlayer(int playerIndex);

    public  List<List<Cell>> play(List<Position> positions);

    public List<Move> getAllMove(Position position);
}
