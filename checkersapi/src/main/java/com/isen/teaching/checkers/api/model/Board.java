package com.isen.teaching.checkers.api.model;

import com.isen.teaching.checkers.api.exception.UnauthorizedMoveException;
import com.isen.teaching.checkers.api.model.cell.Cell;
import com.isen.teaching.checkers.api.model.cell.Cells;
import com.isen.teaching.checkers.api.model.move.Move;
import com.isen.teaching.checkers.api.model.pawn.Pawn;
import org.codehaus.jackson.annotate.JsonIgnore;

public class Board{


	private Cells checkerBoard;

	@JsonIgnore
	private Player playerUp;
	@JsonIgnore
	private Player playerDown;
	@JsonIgnore
    private BoardManager boardManager;

    public Board() {
    }

    public Board(Integer size, Integer pawnNumber) {

		int col = size, row = size;

		this.initCheckerBoard(col, row);
		this.initUsers(pawnNumber);
		this.initPawns(pawnNumber, col, row);
	}

	private void initPawns(Integer pawnNumber, int cols, int rows) {
		this.initPawnsPlayerDown(pawnNumber, cols, rows);
		this.initPawnsPlayerUp(pawnNumber, cols, rows);
	}

	//Joueur en bas jouant vers le haut
	private void initPawnsPlayerDown(Integer pawnNumber, int cols, int rows) {
        for (int row = 0; row < pawnNumber; row++) {
            fillColumnWithPawns(this.playerUp, row);
        }
	}


	//joueur en haut jouant vers le bas
    private void initPawnsPlayerUp(Integer pawnNumber, int cols, int rows) {
        for (int row = this.checkerBoard.getLastRowIndex(); row > this.checkerBoard.getLastRowIndex() - pawnNumber; row--) {
            fillColumnWithPawns(this.playerDown, row);
        }
	}

    private void fillColumnWithPawns(Player player, int row) {
        for (int column = 0; column < this.checkerBoard.getNbColumns(); column++) {
            Cell cell = checkerBoard.get(row, column);
            if (cell.getColor() == Color.BLACK) {
                cell.setCurrentPawn(player.newPawn(player.getColorPawn(), player.getPawnDirection()));
            }
        }
    }

    private void initUsers(Integer pawnNumber) {
		this.playerUp = new Player(pawnNumber, Color.RED, Direction.DOWN, 0);
		this.playerDown = new Player(pawnNumber, Color.BLUE, Direction.UP, this.checkerBoard.getLastRowIndex());
	}

	private void initCheckerBoard(int col, int row) {
		this.checkerBoard = new Cells(col, row);
        this.boardManager = new BoardManager(checkerBoard);
	}

    public void play(Position origin, Position destination)
    {
        final Cell originCell = getCell(origin.getColumnIndex(), origin.getRowIndex());
        final Cell destinationCell = getCell(destination.getColumnIndex(), destination.getRowIndex());

        if (originCell.hasPawn())
        {
            this.movePawn(originCell, destinationCell);
        }
    }

    public void movePawn(Cell origin, Cell destination) {
        try {
            Move move = this.boardManager.move(origin, destination);
            checkPawnToDelete(move);
            checkTransformToQueen(move);
        } catch (UnauthorizedMoveException e)
        {
            e.printStackTrace();// TODO
        }
    }

    private void checkPawnToDelete(Move move) {
        if (move.hasPawnToDelete()) {
            Cell pawnCellToDelete = move.getPawnCellToDelete();
            removeUserPawn(pawnCellToDelete.getCurrentPawn());
            pawnCellToDelete.setCurrentPawn(null);
        }
    }

    public void checkTransformToQueen(Move move) {
        Pawn pawn = move.getDestination().getCurrentPawn();
        int row = move.getDestination().getPosition().getRowIndex();
        if ((pawn.getColor() == Color.RED && row == this.playerUp.getQueenRow()) || (pawn.getColor() == Color.BLUE && row == this.playerDown.getQueenRow())) {
            pawn.toQueen();
        }
    }

    private void removeUserPawn(Pawn pawnToDelete) {
        if (pawnToDelete.getColor() == Color.RED) {
            playerUp.removePawn(pawnToDelete);
        } else {
            playerDown.removePawn(pawnToDelete);
        }
    }

	public Cells getCheckerBoard() {
		return checkerBoard;
	}

	public Cell getCell(int col, int row){
		return this.checkerBoard.get(col, row);
	}

	public Player getPlayerUp() {
		return playerUp;
	}

	public Player getPlayerDown() {
		return playerDown;
	}

    public BoardManager getBoardManager() {
        return boardManager;
    }
}
