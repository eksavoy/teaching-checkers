# checkers

Ce projet rassemble la partie backend du jeu de dame

/!\ Les règles de déplacement ne fonctionne pas totalement, le déplacement des pions est opérationnel

## Checkers Api

Le module api contient l'ensemble de l'intelligence ainsi que les modèles de données.

## Checkers Core

Ce module permet d'exposer une api permettant au front de récupérer les données a afficher

Procèdure de lancement et d'installation :
- install du module Api puis du module Core (Le module JPA ne fonctionne pas encore)
- Run du module core

API :
- GET `localhost:8080/cheackers-core/api/game/board` Récupération du plateau de jeu
- GET `localhost:8080/cheackers-core/api/game/player/{id}` Récupération des informations du joueur, les id sont 1 ou 2
- POST `localhost:8080/cheackers-core/api/game/moves` Permet de récupérer la liste des movoments possible pour une position le body doit être `{"columnIndex": 3, "rowIndex": 0 }`
- POST `localhost:8080/cheackers-core/api/game/play` Permet le déplacement d'un pion le body doit être `[
                                                                                                          {
                                                                                                            "columnIndex": 2,
                                                                                                            "rowIndex": 1
                                                                                                          },
                                                                                                          {
                                                                                                            "columnIndex": 3,
                                                                                                            "rowIndex": 0
                                                                                                          }
                                                                                                        ]`

## Checkers JPA

Le module JPA est présent dans le projet avec les tests, mais il n'est pas branché au backend (problème de configuration de la base de donnée).

## Checkers front
Le front du jeu de dame est sur une autre projet : https://gitlab.com/eksavoy/TeachingCherkersFront
