package com.isen.teaching.checkers.core.game;

import com.isen.teaching.checkers.core.PropertiesUrl;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

/**
 * Created by A613792 on 16/01/2017.
 */
public class GameFactory {

    public Game getCheckersGame() throws IOException {
        Properties properties = new Properties();

        URL url = getClass().getClassLoader().getResource(PropertiesUrl.PROPERTIES_URL);
        properties.load(url.openStream());

        return new GameImpl();
    }
}
