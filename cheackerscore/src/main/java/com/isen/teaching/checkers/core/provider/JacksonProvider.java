package com.isen.teaching.checkers.core.provider;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

/**
 * Created by A613792 on 23/01/2017.
 */

@Provider
public class JacksonProvider implements ContextResolver<ObjectMapper> {

    final ObjectMapper mapper;

    public JacksonProvider() {
        this.mapper = createObjectMapper();
    }

    @Override
    public ObjectMapper getContext(Class<?> aClass) {
        return mapper;
    }

    private static ObjectMapper createObjectMapper(){
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationConfig.Feature.INDENT_OUTPUT, true);
        return objectMapper;
    }
}
