package com.isen.teaching.checkers.core.game;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.isen.teaching.checkers.core.database.InitDatabase;

/**
 * Created by A613792 on 16/01/2017.
 */
public class GameSingleton  {

    private static Game GAME = null;

    private GameSingleton() {
    }

    public static synchronized Game getInstance(){
        if(GAME == null){
            Injector injector = Guice.createInjector(new GameModule());
            GAME = injector.getInstance(GameImpl.class);
            //InitDatabase initDatabase = new InitDatabase();
            GAME.run();
        }
        return GAME;
    }
}
