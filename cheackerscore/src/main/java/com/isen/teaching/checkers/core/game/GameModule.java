package com.isen.teaching.checkers.core.game;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.isen.teaching.checkers.core.PropertiesUrl;
import org.apache.log4j.Logger;

import javax.inject.Named;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;


/**
 * Created by A613792 on 16/01/2017.
 */
public class GameModule extends AbstractModule {

    private static Logger logger = Logger.getLogger(GameModule.class);

    @Provides
    @Singleton
    public Game getInstance(@Named("checkers.application.instance") String className){
        try {
            Class<?> instance = Class.forName(className);
            return (Game) instance.newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            logger.error("Unable to find game implementation", e);
            //System.out.println("Unable to find Game implementation");
        }
        return null;
    }

    @Override
    protected void configure() {
        try {
            Names.bindProperties(binder(), getProperties());
        } catch (IOException e) {
            logger.error("Unable to find properties", e);
            //System.out.println("Unable to find properties");
        }
    }

    public Properties getProperties() throws IOException {
        Properties properties = new Properties();
        URL url = getClass().getClassLoader().getResource(PropertiesUrl.PROPERTIES_URL);
        properties.load(url.openStream());
        return properties;
    }
}
