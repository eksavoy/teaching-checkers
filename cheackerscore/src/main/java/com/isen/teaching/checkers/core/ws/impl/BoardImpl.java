package com.isen.teaching.checkers.core.ws.impl;


import com.isen.teaching.checkers.api.model.Board;
import com.isen.teaching.checkers.api.model.Player;
import com.isen.teaching.checkers.api.model.Position;
import com.isen.teaching.checkers.api.model.cell.Cell;
import com.isen.teaching.checkers.api.model.move.Move;
import com.isen.teaching.checkers.api.ws.BoardApi;
import com.isen.teaching.checkers.core.exception.NoFoundException;
import com.isen.teaching.checkers.core.game.GameImpl;
import com.isen.teaching.checkers.core.game.GameSingleton;

import javax.servlet.http.HttpServlet;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by A613792 on 16/01/2017.
 */
@Path("/game")
public class BoardImpl implements BoardApi {

    @Override
    @GET
    @Path("/board")
    @Produces(MediaType.APPLICATION_JSON)
    public List<List<Cell>> getBoard(){
        return ((GameImpl)GameSingleton.getInstance()).getBoard().getCheckerBoard().getCells();
    }

    @Override
    @GET
    @Path("/player/{id}")
    public Player getPlayer(@PathParam("id") int playerIndex) {
        switch (playerIndex){
            case 1:
                return ((GameImpl)GameSingleton.getInstance()).getBoard().getPlayerDown();
            case 2:
                return ((GameImpl)GameSingleton.getInstance()).getBoard().getPlayerUp();
            default:
                break;
        }
        return null;
    }

    @Override
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/play")
    public  List<List<Cell>> play(List<Position> positions) {
        ((GameImpl)GameSingleton.getInstance()).getBoard().play(positions.get(0), positions.get(1));
        return ((GameImpl)GameSingleton.getInstance()).getBoard().getCheckerBoard().getCells();
    }

    @Override
    @POST
    @Path("/moves")
    @Consumes(MediaType.APPLICATION_JSON)
    public List<Move> getAllMove(Position position) {
        List<Move> moves = ((GameImpl)GameSingleton.getInstance()).getBoard().getBoardManager().getPossibleMoves(((GameImpl)GameSingleton.getInstance()).getBoard().getCell(position.getColumnIndex(),position.getRowIndex()));
        return moves;
    }
}
