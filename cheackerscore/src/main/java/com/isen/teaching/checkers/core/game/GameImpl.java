package com.isen.teaching.checkers.core.game;

import com.google.inject.Inject;
import com.isen.teaching.checkers.api.model.Board;

import javax.inject.Named;

/**
 * Created by A613792 on 16/01/2017.
 */
public class GameImpl implements Game {

    @Inject
    @Named("board.size")
    Integer boardSize;

    @Inject
    @Named("discs.number")
    Integer discsNumber;

    private Board board;

    @Override
    public void init() {
        this.board = new Board(this.getSize(), this.getDiscsNumber());
    }

    @Override
    public void run() {
        this.init();
    }

    @Override
    public Integer getSize() {
        return boardSize;
    }

    @Override
    public Integer getDiscsNumber() {
        return discsNumber;
    }

    public Board getBoard() {
        return board;
    }
}
