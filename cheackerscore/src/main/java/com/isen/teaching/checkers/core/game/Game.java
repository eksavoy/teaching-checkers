package com.isen.teaching.checkers.core.game;

/**
 * Created by A613792 on 16/01/2017.
 */
public interface Game {

    public void init();
    public void run();

    public Integer getSize();
    public Integer getDiscsNumber();

}
