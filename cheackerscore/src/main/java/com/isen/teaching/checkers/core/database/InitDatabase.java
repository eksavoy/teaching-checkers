package com.isen.teaching.checkers.core.database;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by A613792 on 22/02/2017.
 */
public class InitDatabase {

    private static final String DB_DRIVER = "org.h2.Driver";
    private static final String DB_CONNECTION = "jdbc:h2:~/test";
    private static final String DB_USER = "";
    private static final String DB_PASSWORD = "";


    public InitDatabase() {

        Connection connection = getDbConnection();
        String createPlayer = "CREATE TABLE player(id int primary key, name varchar(50))";
        String createMove = "CREATE TABLE move(id int primary key, source varchar(50), destination varchar(50), foreign key (player_id) references player.id)";
        PreparedStatement createPreparedStatementPlayer = null;
        PreparedStatement createPreparedStatementMove = null;

        try {
            connection.setAutoCommit(false);
            createPreparedStatementPlayer = connection.prepareStatement(createPlayer);
            createPreparedStatementPlayer.execute();
            createPreparedStatementPlayer.close();
            createPreparedStatementMove = connection.prepareStatement(createMove);
            createPreparedStatementMove.execute();
            createPreparedStatementMove.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private Connection getDbConnection() {
        Connection dbConnection = null;
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        try {
            dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER,
                    DB_PASSWORD);
            return dbConnection;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dbConnection;
    }
}
