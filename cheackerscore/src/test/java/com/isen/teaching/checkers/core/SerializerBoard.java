/*
package com.isen.teaching.checkers.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isen.teaching.checkers.api.model.Board;
import com.isen.teaching.checkers.core.game.GameImpl;
import com.isen.teaching.checkers.core.game.GameSingleton;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

*/
/**
 * Created by A613792 on 16/01/2017.
 *//*

public class SerializerBoard {

    private static Logger logger = Logger.getLogger(BoardInitialiseTest.class);

    private int size = 10;
    private int nbPawn = 12;
    private int nbCols = size;
    private int nbRows = size;
    private BoardApi board;

    @Before
    public void init(){
        board = new BoardApi(size, nbPawn);
    }

    @Test
    public void can_serializer_board(){
        ObjectMapper mapper = new ObjectMapper();
        mapper = mapper.clearProblemHandlers();
        String json = null;
        try {
            json = mapper.writeValueAsString(GameSingleton.getInstance());

        } catch (JsonProcessingException e) {
            logger.error("Error json processing", e);
            json = e.getMessage();
        }
        logger.debug(json);
        assertThat(json).isNotEmpty();
    }
}
*/
