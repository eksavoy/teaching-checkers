package com.isen.teaching.checkers.core;

import com.isen.teaching.checkers.api.model.Board;
import com.isen.teaching.checkers.api.model.Color;
import com.isen.teaching.checkers.api.model.Player;
import com.isen.teaching.checkers.api.model.cell.Cell;
import com.isen.teaching.checkers.api.model.cell.Cells;
import com.isen.teaching.checkers.api.model.pawn.Pawn;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class BoardInitialiseTest {

    private static Logger logger = Logger.getLogger(BoardInitialiseTest.class);

    private int size = 10;
    private int nbPawn = 4;
    private int nbCols = size;
    private int nbRows = size;
    private Board board;

    @Before
    public void doBefore() throws Exception {
        this.board = new Board(size, nbPawn);
    }

    @Test
    public void isBoardInitialized() throws Exception {
        logger.info("[isBoardInitialized] BEGIN");
        assertThat(board.getCheckerBoard()).isNotNull();
        assertThat(board.getCheckerBoard().getNbRows()).isEqualTo(nbRows);
        for (int i = 0; i < nbRows; i++) {
            assertThat(board.getCheckerBoard().getColumn(i).size()).isEqualTo(nbCols);
        }

        logger.info("[isBoardInitialized] BoardApi size =  " + nbRows + "x" + nbCols);
        logger.info("[isBoardInitialized] END");
    }

    @Test
    public void isBoardColorsOk() throws Exception {
        logger.info("[isBoardColorsOk] BEGIN");
        Cells cells = board.getCheckerBoard();

        for (int x = 0; x < nbRows; x++) {
            List<Cell> row = cells.getColumn(x);
            for(int y = 0; y < nbCols; y++) {
                Cell cell = row.get(y);
                Color color;
                if (x % 2 == 0) {
                    color = y % 2 == 0 ? Color.WHITE : Color.BLACK;
                }
                else {
                    color = y % 2 == 0 ? Color.BLACK : Color.WHITE;
                }
                assertThat(cell.getColor()).isEqualTo(color);
            }
        }
        logger.info("[isBoardColorsOk] END");
    }

    @Test
    public void isUsersCellsOk() {
        logger.info("[isUsersCellsOk] BEGIN");
        this.isUserCellsOk(this.board.getPlayerUp());
        this.isUserCellsOk(this.board.getPlayerDown());
        logger.info("[isUsersCellsOk] END");
    }

    private void isUserCellsOk(Player user) {
        assertThat(user.getNbPawns()).isEqualTo(nbPawn);
    }

    @Test
    public void isPawnsWellPlaced() {
        logger.info("[isPawnsWellPlaced] BEGIN");
        for (int row = 0; row < nbRows; row++) {
            for (int column = 0; column < nbCols; column++) {
                Cell currentCell = this.board.getCell(row, column);
                Pawn currentPawn = currentCell.getCurrentPawn();

                if (currentPawn != null) {
                    assertThat(currentCell.getColor()).isEqualTo(Color.BLACK);
                }
            }
            System.out.println();
        }
        logger.info("[isPawnsWellPlaced] END");
    }

}
