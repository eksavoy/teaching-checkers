package com.isen.checkers.jpa;

import java.util.List;

public interface CheckersDAO {

	Player getPlayer(String name);

	String getSource(String source);

	Move getMove(Long id);

	String getDestination(String destination);

	public Player addPlayer(String name);

	public Player savePlayer(Player player);

	public Move addMove(Player player, String source, String dest);

	Move saveMove(Move move);

	public void deleteMove();

	public List<Move> getAllMoveOfOnePlayer();

	void deletePlayer(Player player);

}
