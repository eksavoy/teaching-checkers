package com.isen.checkers.jpa;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;

public class JPAPlayerDAO implements CheckersDAO {

	@Inject
	EntityManager em;

	@Override
	public String getSource(String source) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDestination(String destination) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Player addPlayer(String name) {
		return new Player(name);
	}

	@Override
	public Player savePlayer(Player player) {
		EntityTransaction trans = em.getTransaction();

		trans.begin();
		em.persist(player);
		em.flush();
		trans.commit();
		return player;

	}

	@Override
	public Move addMove(Player player, String source, String dest) {
		return new Move(player, source, dest);
	}

	@Override
	public Move saveMove(Move move) {

		EntityTransaction trans = em.getTransaction();

		trans.begin();
		em.persist(move);
		em.flush();
		trans.commit();
		return move;
	}

	@Override
	public void deleteMove() {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Move> getAllMoveOfOnePlayer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Player getPlayer(String name) {
		try {
			return (Player) em.createQuery("FROM player p WHERE p.name = :name")
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}

	}

	@Override
	public Move getMove(Long id) {
		return em.find(Move.class, id);
	}

	@Override
	public void deletePlayer(Player player) {

		EntityTransaction trans = em.getTransaction();

		trans.begin();
		em.remove(player);
		em.flush();
		trans.commit();

	}

}
