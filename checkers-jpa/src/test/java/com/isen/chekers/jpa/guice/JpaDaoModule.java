package com.isen.chekers.jpa.guice;

import com.google.inject.AbstractModule;
import com.isen.checkers.jpa.CheckersDAO;
import com.isen.checkers.jpa.JPAPlayerDAO;

public class JpaDaoModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(CheckersDAO.class).to(JPAPlayerDAO.class);
		// bind(CategoryDAO.class).to(JPACategoryDAO.class);
	}

}
