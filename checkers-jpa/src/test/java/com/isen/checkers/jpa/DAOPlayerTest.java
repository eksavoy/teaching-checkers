package com.isen.checkers.jpa;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.isen.chekers.jpa.guice.GuiceRunner;
import com.isen.chekers.jpa.guice.H2DBModule;
import com.isen.chekers.jpa.guice.JpaDaoModule;
import com.isen.chekers.jpa.guice.Modules;

@RunWith(GuiceRunner.class)
@Modules({ JpaDaoModule.class, H2DBModule.class })

public class DAOPlayerTest {

	@Inject
	CheckersDAO dao;

	@Test
	public void daoIsInjected() throws Exception {
		assertNotNull(dao);
	}

	@Test
	public void iCanCeateAPlayer() throws Exception {

		// creation d'un premier player
		Player player = dao.addPlayer("John");
		assertThat(player.getName()).isEqualTo("John");

		// save the player
		dao.savePlayer(player);

		// get back the player
		Player playerTest = dao.getPlayer("John");
		assertThat(playerTest.getName()).isEqualTo(player.getName());

	}

	@Test
	public void iCanDeleteAPlayer() throws Exception {

		// creation d'un premier player
		Player player = dao.addPlayer("John");

		// save the player
		dao.savePlayer(player);

		// delete him
		dao.deletePlayer(player);
		Player playerTest = dao.getPlayer("John");
		assertThat(playerTest).isEqualTo(null);
	}

	@Test
	public void iCanMakeAMove() throws Exception {

		Player player = new Player("Romain");
		String source = "1";
		String dest = "2";
		Move move = dao.addMove(player, source, dest);

		Move moveSave = dao.saveMove(move);

		Move moveTest = dao.getMove(moveSave.getId());
		assertThat(move.getPlayer().getName()).isEqualTo("Romain");

	}

}
